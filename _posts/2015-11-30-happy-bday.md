---
layout: post
title:  "happy b'day"
date:   2015-11-30 14:40:00
description: happy b'day
categories:
- j
- happybirthday
permalink: happy-bday
---

<audio controls id="happybirthday-audio">
  <source src="/assets/media/hpbday.mp3" type="audio/mpeg">
  <source src="/assets/media/hpbday.ogg" type="audio/ogg">
  Your browser does not support this audio element.
</audio>

<a href="/assets/media/hpbday.jpg" target="_blank" id="happybirthday-img">
  <img src="/assets/media/hpbday.jpg">
</a>
