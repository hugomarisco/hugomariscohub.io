---
layout: page
title: me?
permalink: /me/
---

[Yo](https://www.justyo.co/), I am Hugo Mendes, another web developer based in [Portugal](https://www.google.pt/search?q=Portugal).

(Almost) a CS graduated from [University of Minho](http://www.uminho.pt).

I've spent a decent part of last decade roaming around with code, algorithms and hardware.

# here are some of my accomplishments

---

#### Full-stack Developer at [ctb.pt](http://www.ctb.pt)

Built all the [Wordpress](https://wordpress.com/) platform and advised on all sysop requirements (hosting, email, log management, etc).

This was a relatively straight-forward project and most of the issues were due to design constraints imposed.

***

#### Full-stack Developer at [ultimateautobuyer.com](https://www.google.pt/search?q=ultimateautobuyer)

Developed the platform sustaining this FIFA Ultimate Team bot for both 2014/2015 versions:

- **2014** - Backend using [rails-api](https://github.com/rails-api/rails-api) and front-end in Java with JavaFX GUI.
- **2015** - Backend using [rails-api](https://github.com/rails-api/rails-api) and front-end in [EmberJS](http://emberjs.com/) embeded in a [node-webkit](http://nwjs.io/) container.

This project involved several challenges like timed events in a multi-threaded environment, various payment options' management (both recurring and one-time) and dealing with data-scraping from EA's platform.

---

#### Backend Developer at [BFUTC.com](http://bfutc.com/)

Another [FIFA Ultimate Team](https://www.easports.com/fifa/ultimate-team/features) related project, this time, a coin (currency used in the game) store hosted in [Shopify](http://www.shopify.com/).

I've built the entire backend that was responsible to fulfill the orders made in Shopify store.

I liked to work on this one, mostly because it introduced me a lot of concepts about queue management and API security.

---

#### Developer at [EVOLEECH.org](https://www.google.pt/webhp?q=evoleech)

This was one of my first projects and consisted on a bittorrent tracker.

It was developed from the scratch using PHP, MySQL, HTML and CSS.

Hypothetically, I might have created a background system that automatically grabbed the content from [FTP scene sites](https://en.wikipedia.org/wiki/Topsite_(warez)), parsed the [NFO](http://www.nfohump.com/), created the torrent and send it to the bittorrent tracker.

***

# contact me (if it's good news)

You can find me on [Google+][google] / [Github][github] / [Twitter][Twitter] / [AngelList][angellist] or just say `Hello` at
**_@hugomarisco.in** (`_` can be anything, really, be creative).

[github]: https://github.com/hugomarisco
[google]: https://plus.google.com/+HugoMarisco
[twitter]: https://twitter.com/hugomarisco
[facebook]: https://facebook.com/je.suis.marisco
[angellist]: https://angel.co/hugomarisco
