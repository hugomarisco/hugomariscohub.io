jQuery(document).ready(function($){
	$('#ytv').ytv({
		playlist: 'PLnJHqFZugXWBDCETGyUjG90NsR4uzQ_aj',
		autoplay: true,
		browsePlaylists: false,
		responsive: true,
		playerTheme: 'light',
		listTheme: 'light',
		reverseList: true,
		viewCount: false,
		events: {
			videoReady: function(data) {
				videoData = data.target.B.videoData;

				videoTitle = videoData.title;

				console.log(videoTitle);

				ga('send', 'event', 'video', 'play', videoTitle);
			}
		}
	});

	$('#happybirthday-audio')[0].onplay = function() {
		ga('send', 'event', 'video', 'play', 'happybday');
	};

	$('#happybirthday-img').click(function() {
		ga('send', 'event', 'image', 'open', 'happybday');
	});
});

